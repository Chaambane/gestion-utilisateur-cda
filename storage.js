let manageApp = {

    //LocalStorage key
    key: "users",

    //Ajouter nouveau utilisateur
    addNewUser: function () {
        //Init localstorage
        if(localStorage.getItem(this.key) == null) {
            localStorage.setItem(this.key, "[]");
        }

        //Charger le contenu du local storage
        let data = JSON.parse(localStorage.getItem(this.key));

        //Structure d'un objet utilisateur
        let user = {
            id: this.createId(),
            username: document.querySelector('#newUser').value,
            tasks: []
        }
        //Ajout de l'utilisateur dans localStorage
        data.push(user);
        localStorage.setItem(this.key, JSON.stringify(data));

        this.getUsersInLocalstorage();

    },
    //Céer un id 
    createId() {
        let id = "";
        let carac = "ABCDEFG&HIJKLM$NOPQRSTUVWXYZ01234_56789";
        for ( var i = 0; i < 15; i++ ) {
            id += carac.charAt(Math.floor(Math.random() * 40));
        }
        return id;
        },
    //Affichage des utilisateurs dans la liste DOM
    getUsersInLocalstorage: function () {
            if (localStorage.getItem(this.key) == null) {
                localStorage.setItem(this.key, "[]");
            }
            let users = JSON.parse(localStorage.getItem(this.key));

            let html = '<option selected>Choix utilisateur</option>'
            for(let u = 0; u < users.length; u++) {
                html += `<option value="${users[u].id}">
                            ${users[u].username}
                        </option>`;
            }
            const usersListe = document.querySelector('#selectUser');
            usersListe.innerHTML = html;
        },
    // //Charger les utilisateurs dans le localStorage
    getAllUsers: function () {
        if (localStorage.getItem(this.key) == null) {
            localStorage.setItem(this.key, "[]");
        }
        return JSON.parse(localStorage.getItem(this.key));
    },
    // //Ajout des taches dans un utilisateur séléctionnée
    addUserTask: function () {
        let userSelect = document.querySelector('#selectUser').value;
        let priorityTask = document.querySelector('#priorityTask').value;
        let task = document.querySelector('#addTask').value;

        let users = this.getAllUsers();

        for(let u = 0; u < users.length; u++) {
            if(users[u].id == userSelect) {
                    let taskUser = {
                        id: users[u].tasks.length,
                        description: task,
                        priority: priorityTask,
                        completed: false
                    };

                users[u].tasks.push(taskUser);
                break;
            }
        }
        //Mise à jour localStorage
        localStorage.setItem(this.key, JSON.stringify(users));
    },
    getTaskUserSelect: function () {
        let taskRow = document.querySelector('#myTasks');
        let userSelect = document.querySelector('#selectUser').value;
        console.log(userSelect);
        let users = this.getAllUsers();
        console.log(users);
        
        let html = '';
        for(let u = 0; u < users.length; u++) {
            for(let t = 0; t < users[u].tasks.length; t++) {
                console.log(users[u].tasks[t]);
                html += "<tr id='" + users[u].tasks[t].id + "'>";
                            html += "<th scope='row'>" + users[u].tasks[t].id + "</th>";
                            if(users[u].tasks[t].priority) {
                                html += "<td class='text-success fw-bold'>" + users[u].tasks[t].description + "</td>";
                            } else {
                                html += "<td class='text-danger fw-bold'>" + users[u].tasks[t].description + "</td>";
                            }
                            html += `<th>
                                <button class="btn btn-success">&#10003;</button>
                                <button class="btn btn-danger">X</button>
                            </th>
                        </tr>`;
                    }
                    taskRow.innerHTML = html;
                    //Mise à jour localStorage
                    localStorage.setItem(this.key, JSON.stringify(users));
                    this.getUsersInLocalstorage();
                }
            }

};

//Démarrage de l'application
window.addEventListener("load", function () {
    manageApp.getUsersInLocalstorage();
    manageApp.getTaskUserSelect();
});
