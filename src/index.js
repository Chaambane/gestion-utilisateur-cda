// BDD de départ___________________________________________________________________________________
var jsonDatabase =
    [
        {
            "selectedUser":
            {
                "_id": 0000,
                "username": "",
                "tasks": []
            }
        },
        {
            "users":
                [
                    {
                        "id": 0001,
                        "username": "DevThomas",
                        "tasks": [
                            {
                                "idTask": 1,
                                "priority": true,
                                "description": "Coder c'est bien"
                            },
                            {
                                "idTask": 2,
                                "priority": true,
                                "description": "Acheter les fleurs pour ma femme"
                            },
                            {
                                "idTask": 3,
                                "priority": false,
                                "description": "Coder à la plage"
                            }
                        ]
                    },
                    {
                        "id": 0002,
                        "username": "DevChaambane",
                        "tasks": [
                            {
                                "idTask": 1,
                                "priority": false,
                                "description": "Faire les course"
                            },
                            {
                                "idTask": 2,
                                "priority": true,
                                "description": "Revoire le Python"
                            },
                            {
                                "idTask": 3,
                                "priority": false,
                                "description": "Sql c'est light"
                            }
                        ]
                    }
                ]
        }
    ];


// Initialisation de la base de données avec 'localStorage'
localStorage.setItem('objDatabase', JSON.stringify(jsonDatabase));
var database = JSON.parse(localStorage.objDatabase);

// // Insertion d'une tâche test
// var inputName = 'DevThomas'; // Utilisateur sélectionné
// var inputTask = {           // Tâche à valider
//     "idTask": 1,
//     "priority": false,
//     "description": "Texte de la tâche ajoutée"
// };
// var database = JSON.parse(localStorage.objDatabase);
// for (var i = 0; i < database.length; i++) {
//     if (inputName === database[1].users[i].username) {
//         database[1].users[i].tasks.push(inputTask);
//         break;
//     }
// }
// // Ecriture
// localStorage.setItem("objDatabase", JSON.stringify(database));
// // console.log(database);
// // Lecture
// // let objJson = JSON.parse(localStorage.getItem("objDatabase"));
// // console.log(objJson);


// Affichage des utilisateurs dans les options avec select_________________________________________
let objJson = JSON.parse(localStorage.getItem("objDatabase"));
let html = `<option selected hidden>Utilisateur</option>`;
let segment = '';
for (var i = 0; i < database[1].users.length; i++) {
    segment = `<option id="${database[1].users[i].id}" class="user-name">${database[1].users[i].username}</option>`;
    html += segment;
}
const userList = document.querySelector("#user-list");
userList.innerHTML = html;
html = '';
segment = '';
userNameEvent();


// Actions sur l'utilisateur selectionné___________________________________________________________
const taskTodo = document.querySelector('#task-todo');

function userNameEvent() {
    const userNameH1 = document.querySelector("#user-name");
    document.querySelectorAll('.user-name').forEach(function (item) {
        item.addEventListener("click", function () {
            taskTodo.innerHTML = '';
            getDataOneUser(item.id);
            userNameH1.innerText = item.innerText;
        })
    })
};

// Récupération des tâches de l'utilisateur
function getDataOneUser(id) {

    let objJson = JSON.parse(localStorage.getItem("objDatabase"));
    for (var i = 0; i < database[1].users.length; i++) {
        if (database[1].users[i].id == id) {

            database[1].users[i].tasks.forEach(function (task) {
                let segment = `
                    <div class="task-row">
                        <textarea class="input-text ${task.priority}">${task.description}</textarea>
                        <input type="checkbox" class="checkbox">
                    </div>`;
                html += segment; 
            })
        }
    }

    // let tasksUser = doc.data().tasks;
    // let html = '';
    // tasksUser.map(task => {
    //     // console.log(task.description);
    //     let segment = `
    //                 <div class="task-row">
    //                     <textarea class="input-text ${task.priority}">${task.description}</textarea>
    //                     <input type="checkbox" class="checkbox">
    //                 </div>`;
    //     html += segment
    // });
    taskTodo.innerHTML = html;
    html = '';
}

// Adding user in db with popup
// const addUserBtn = document.querySelector('.add-username');
// const addUser = document.querySelector('.add');
// const addUserForm = document.querySelector('.add form');
// addUserBtn.addEventListener('click', () => {
//     addUser.classList.add("active");
//     addUserForm.addEventListener('submit', (e) => {
//         e.preventDefault();

//         addDoc(colRef, {
//             username: addUserForm.username.value,
//             tasks: [],
//         })
//             .then((onfulfilled) => {
//                 addUser.classList.remove('active');
//                 addUserForm.reset();
//             }, (onrejected) => {
//                 console.log(onrejected);
//             });
//     });
// });

//Remove popup
const popup = document.querySelector('.popup');
window.addEventListener('click', (e) => {
    if (e.target == popup) {
        popup.classList.remove('active');
        addUserForm.reset();
    }
});

// Ajout d'une tâche (champ saisie) avec couleur (priorité)______________________________________________________
const taskPriority = [document.querySelector(".normal"), document.querySelector(".urgent")];
const taskCategorie = document.querySelector('.task-categorie');

taskPriority.forEach(function (item) {
    if (item.innerHTML === 'Normale') {
        var priority = 'normal';
    } else if (item.innerHTML === 'Urgente') {
        var priority = 'urgent';
    };
    item.addEventListener('click', () => {
        item.parentNode.selectedIndex = 0;
        addTasks(priority);
    });
});

function addTasks(priority) {
    // const userSelectedId = localStorage.getItem("userSelectedId");
    // const docRef = doc(db, 'users', userSelectedId);
    let html = '';
    if (priority === 'normal') {
        html = `<textarea
                  class="input-text normal">Valider en cliquant en dehors de la zone de saisie !</textarea>
              <input type="checkbox" class="checkbox">`;
        // Actions sur la BDD
        updateDoc(docRef, {
            tasks: arrayUnion(
                {
                    description: '',
                    done: false,
                    priority: 'normal'
                })
        });
    } else if (priority === 'urgent') {
        html = `<textarea
                  class="input-text urgent">Valider en cliquant en dehors de la zone de saisie !</textarea>
              <input type="checkbox" class="checkbox">`;
        // Actions sur la BDD
        updateDoc(docRef, {
            tasks: arrayUnion(
                {
                    description: '',
                    done: false,
                    priority: 'urgent'
                })
        });
    }
    let elem = document.createElement('div');
    elem.innerHTML = html;
    elem.setAttribute('class', 'task-row');
    function insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    };
    insertAfter(taskCategorie, elem);
    addValidationTaskEvent(elem.firstChild);
};

// Valider la saisie d'une tache en cliquant en dehors (à côté)____________________________________
function addValidationTaskEvent(elem) {
    elem.addEventListener("click", function () {
        document.addEventListener("click", function (event) {
            let targetEl = event.target; // élément cliqué    
            do {
                if (targetEl == elem) {
                    // click interne
                    return;
                }
                targetEl = targetEl.parentNode;
            } while (targetEl);
            elem.innerText = `Saisie validée !`;
        });
    });
};
document.querySelectorAll('textarea').forEach(function (elem) {
    addValidationTaskEvent(elem);
});

// Supprimer textarea si checked => clic bouton supprimer__________________________________________
const delTasks = document.querySelector('#delTasks');
delTasks.addEventListener("click", () => {
    deleteTasks();
});
function deleteTasks() {
    document.querySelectorAll('.checkbox:checked').forEach(function (item) {
        //shake
        item.parentElement.classList.add("apply-shake");
        item.parentElement.addEventListener("animationend", (e) => {
            setTimeout(function () { item.parentElement.remove() }, 500);
            item.parentElement.remove();
        });
    });


    // Actions sur la BDD





};


// Filtrage des tâches_____________________________________________________________________________
const taskFilter = [document.querySelector(".noFilter"), document.querySelector(".normalFilter"),
document.querySelector(".urgentFilter")];
taskFilter.forEach(function (item) {
    if (item.innerHTML === 'Tout') {
        var filter = 'all';
    } else if (item.innerHTML === '| Normales') {
        var filter = 'normals';
    } else if (item.innerHTML === '| Urgentes') {
        var filter = 'urgents';
    };
    item.addEventListener('click', () => {
        item.parentNode.selectedIndex = 0;
        tasksFilter(filter);
    });
});

function tasksFilter(filter) {
    if (filter === 'all') {
        document.querySelectorAll('.normal, .urgent').forEach(function (item) {
            item.parentElement.style.display = "flex";
        });
    } else if (filter === 'normals') {
        document.querySelectorAll('.urgent').forEach(function (item) {
            item.parentElement.style.display = "none";
        });
        document.querySelectorAll('.normal').forEach(function (item) {
            item.parentElement.style.display = "flex";
        });
    } else if (filter === 'urgents') {
        document.querySelectorAll('.normal').forEach(function (item) {
            item.parentElement.style.display = "none";
        });
        document.querySelectorAll('.urgent').forEach(function (item) {
            item.parentElement.style.display = "flex";
        });
    }
};


// Basculement [>] et [<] des tâches_______________________________________________________________
// Définitions des boutons de basculement [>] et [<]
const doneTask = document.querySelector('#doneTask');
const todoTask = document.querySelector('#todoTask');
// Liste de taches à faire (gauche) et faites (droite)
const todoList = document.querySelector('#todoList');
const doneList = document.querySelector('#doneList');
// Zones insertion pour 'insertafter'
let insertTodo = document.querySelector('#insertTodo');
let insertDone = document.querySelector('#insertDone');
// Appels des fonctions au clics des boutons [>] et [<]
doneTask.addEventListener("click", () => {
    doneTasks();
});
todoTask.addEventListener("click", () => {
    todoTasks();
});

// Fonctions de déplacement des taches (textarea + checbox => task-row)
function doneTasks() {
    todoList.querySelectorAll('.checkbox:checked').forEach(function (item) {
        let task = item.parentElement;
        task.removeChild(item);
        task.prepend(item);
        let elem = document.createElement('div');
        elem.innerHTML = task.outerHTML;
        function insertAfter(referenceNode, newNode) {
            referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
        };
        insertAfter(insertDone, elem);
        task.remove();
    });
};
function todoTasks() {
    doneList.querySelectorAll('.checkbox:checked').forEach(function (item) {
        let task = item.parentElement;
        task.removeChild(item);
        task.appendChild(item);
        let elem = document.createElement('div');
        elem.innerHTML = task.outerHTML;
        function insertAfter(referenceNode, newNode) {
            referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
        };
        insertAfter(insertTodo, elem);
        task.remove();
    });
};
