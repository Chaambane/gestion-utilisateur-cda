Vous devez créer une application permettant de gérer plusieurs utilisateurs.
Chacun aura sa propre liste de tâche.

CONTRAINTES:

 • Au niveau de l'interface graphique, il faudra pouvoir sélectionner un utilisateur: ses tâches seront alors accessibles

 • Une tâche pourra être créée/catégorisée/faite/supprimée

 • Les tâches devront être affichée en 2 blocs: les taches à faire et faites

 • Un filtre permettra d'afficher uniquement les tâches d'une même catégorie

 • Les tâches devront être persistantes (localStorage ou mieux: BDD (via firebase ou un seveur perso) )